import { check } from 'express-validator';
import { hourRegex } from '../helpers/helpers.js';
import { validateResult } from '../helpers/validateHelper.js';

export const punchCreateCheck = () => {
  return [
    check('userId').trim().isInt().withMessage
      ('El id del usuario es incorrecto').not().isEmpty().withMessage('El id del usuario a actualizar es obligatorio'),
    check('user_name').isString().not().isEmpty().withMessage
      ('El nombre de usuario no puede estar vacio'),
    check('date').trim().not().isEmpty().withMessage
      ('El parametro punch_in no puede estar vacio').isDate().withMessage
      ('Debe de ingresar una fecha valida'),
    check('punch_in').trim().not().isEmpty().withMessage
      ('El parametro punch_in no puede estar vacio').custom((value, { req }) => {
        if (!hourRegex.test(value)) throw new Error('El formato de la hora debe ser HH:SS (24h)');
        return true;
      }),
    check('punch_out').trim().not().isEmpty().withMessage
      ('El parametro punch_out no puede estar vacio').custom((value, { req }) => {
        if (!hourRegex.test(value)) throw new Error('El formato de la hora debe ser HH:SS (24h)');
        return true;
      }),
    (req, res, next) => {
      validateResult(req, res, next)
    }
  ]
}

export const punchUpdateCheck = () => {
  return [
    check('id').trim().isInt().withMessage
      ('El id del punch es incorrecto').not().isEmpty().withMessage('El id del punch a actualizar es obligatorio'),
    check('userId').trim().isInt().withMessage
      ('El id del usuario es incorrecto').not().isEmpty().withMessage('El id del usuario a actualizar es obligatorio'),
    check('user_name').isString().not().isEmpty().withMessage
      ('El nombre de usuario no puede estar vacio'),
    check('date').trim().not().isEmpty().withMessage
      ('El parametro punch_in no puede estar vacio').isDate().withMessage
      ('Debe de ingresar una fecha valida'),
    check('punch_in').trim().not().isEmpty().withMessage
      ('El parametro punch_in no puede estar vacio').custom((value, { req }) => {
        if (!hourRegex.test(value)) throw new Error('El formato de la hora debe ser HH:SS (24h)');
        return true;
      }),
    check('punch_out').trim().not().isEmpty().withMessage
      ('El parametro punch_out no puede estar vacio').custom((value, { req }) => {
        if (!hourRegex.test(value)) throw new Error('El formato de la hora debe ser HH:SS (24h)');
        return true;
      }),
    (req, res, next) => {
      validateResult(req, res, next)
    }
  ]
}