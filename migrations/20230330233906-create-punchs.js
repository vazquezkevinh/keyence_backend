'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('punchs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      user_name: {
        type: Sequelize.STRING
      },
      date: {
        type: Sequelize.DATEONLY
      },
      punch_in: {
        type: Sequelize.TIME
      },
      punch_out: {
        type: Sequelize.TIME
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('punchs');
  }
};