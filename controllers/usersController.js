import { hash } from 'bcrypt';

import { User } from '../models/user.js';

export const getAllUsers = async (req, res) => {
    const users = await User.findAll({
        attributes: ['id', 'username', 'nombre', 'email', 'active']
    });
    res.json(users);
}

export const verify = async (req, res) => {
    return res.status(200).json({ 'message': 'Sesión verificada' });
}

export const storeUser = async (req, res) => {
    const { user, pwd, name, rol } = req.body;
    if (!user || !pwd || !name || !rol) return res.status(400).json({ 'message': 'Por favor verifica la información ingresada.' });

    // check for duplicate usernames in the db
    const duplicate = await User.findOne({ where: { username: user } });
    if (duplicate) return res.status(409).json({ "message": 'El usuario ingresado ya se encuentra registrado' });

    try {
        //encrypt the password
        const hashedPwd = await hash(pwd, 10);

        //create and store the new user
        const result = await User.create({
            "username": user,
            "password": hashedPwd,
            "nombre": name
        });

        res.status(201).json({ 'success': `New user ${user} created!` });
    } catch (err) {
        res.status(500).json({ 'message': err.message });
    }
}

export const getUserData = async (req, res) => {
    const { id } = req.body;
    if (!id) return res.status(400).json({ 'message': 'Por favor verifica la información ingresada.' });

    const findInfo = await User.findOne({
        where: { id: id },
        attributes: ['id', 'username', 'nombre', 'email', 'active']
    });

    if (!findInfo) return res.status(204).json({ "message": 'El usuario ingresado no fue localizado' });

    res.json({ user: findInfo });
}

export const updateUser = async (req, res) => {
    const { id, pwd, name, rol, isActive } = req.body;
    if (!name || !rol) return res.status(400).json({ 'message': 'Por favor verifica la información ingresada.' });

    // check for duplicate usernames in the db
    const foundUser = await User.findOne({ where: { id } });
    if (!foundUser) return res.status(409).json({ "message": 'El usuario no fue localizado' });

    try {
        //encrypt the password
        const hashedPwd = pwd ? await hash(pwd, 10) : foundUser.password;

        //create and store the new user
        foundUser.remember_token = '';
        foundUser.password = hashedPwd;
        foundUser.nombre = name;
        foundUser.active = isActive ?? 0;
        const result = await foundUser.save();

        res.status(201).json({ 'success': `Usuario actualizado correctamente` });
    } catch (err) {
        res.status(500).json({ 'message': err.message });
    }
}