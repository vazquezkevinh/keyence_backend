import { compare } from 'bcrypt';
import jsonwebtoken from 'jsonwebtoken';
import { Op } from "sequelize";
import { User } from '../models/user.js';
import { UserPermissions } from '../models/userpermissions.js';

const { sign, decode, verify } = jsonwebtoken;

export const handleLogin = async (req, res) => {
    const { user, pwd } = req.body;
    if (!user || !pwd) return res.status(400).json({ 'message': 'Username and password are required.' });

    const foundUser = await User.findOne({
        where: { username: user }
    });
    if (!foundUser) return res.sendStatus(401); //Unauthorized 
    // evaluate password 
    const match = await compare(pwd, foundUser.password);
    if (match) {

        const privilegios = await UserPermissions.findAll({ where: { usuario_id: foundUser.id }, attributes: ['priv_name'], raw: true })
        const privs = privilegios.map((data) => { return data.priv_name })

        // create JWTs
        const accessToken = sign(
            {
                "UserInfo": {
                    "username": foundUser.username,
                    "privilegios": privs,
                }
            },
            process.env.ACCESS_TOKEN_SECRET,
            { expiresIn: '5h' }
        );
        const refreshToken = sign(
            {
                "UserInfo": {
                    "username": foundUser.username,
                    "privilegios": privs
                }
            },
            process.env.REFRESH_TOKEN_SECRET,
            { expiresIn: '1d' }
        );
        // Saving refreshToken with current user
        foundUser.remember_token = refreshToken;
        const result = await foundUser.save();


        // Creates Secure Cookie with refresh token
        res.cookie('jwt', refreshToken, { httpOnly: true, secure: true, sameSite: 'None', maxAge: 24 * 60 * 60 * 1000 });

        // Send authorization roles and access token to user
        res.json({ roles: privs, accessToken, user: foundUser.username });

    } else {
        res.sendStatus(401);
    }
}

export const verifyByToken = async (req, res, next) => {
    const authHeader = req.headers.authorization || req.headers.Authorization;
    if (!authHeader?.startsWith('Bearer ')) return res.sendStatus(401);
    const token = authHeader.split(' ')[1];

    jsonwebtoken.verify(
        token,
        process.env.ACCESS_TOKEN_SECRET,
        (err, decoded) => {
            console.log(decoded)
            if (err) return res.sendStatus(403); //invalid token
            res.json({
                roles: decoded.UserInfo.privilegios,
                user: decoded.UserInfo.username
            });
        }
    );
}