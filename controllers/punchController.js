import Excel from 'exceljs';
import path from 'path';
import { fileURLToPath } from 'url';
import { Punch } from "../models/punch.js";

/* Helpers */
import { dateRegex, hourRegex, isValidDate, isValidHour } from '../helpers/helpers.js';

const __filename = fileURLToPath(import.meta.url);

export const getAllPunch = async (req, res, next) => {
  try {
    const punchs = await Punch.findAll();
    res.status(200).json(punchs);
  } catch (error) {
    next(error);
  }
}

export const getPunch = async (req, res, next) => {
  const { id } = req.params;
  try {
    //check if params are correct
    if (!id) return res.status(400).json({ message: 'Por favor verifica la información ingresada.', code: "PchCtr-002", status: 400 });
    //check for existing punch
    const punch = await Punch.findOne({
      where: { id: id },
      attributes: ['id', 'user_name', 'date', 'punch_in', 'punch_out']
    });
    if (!punch) return res.status(204).json({ message: 'El usuario ingresado no fue localizado', code: "PchCtr-003", status: 204 });
    res.status(200).json({ punch });
  } catch (error) {
    next(error);
  }
}

export const createPunch = async (req, res, next) => {
  const { userId, date, punch_in, punch_out, user_name } = req.body;
  try {
    const punch = await Punch.create({
      userId: userId,
      user_name: user_name,
      date: date,
      punch_in: punch_in,
      punch_out: punch_out,
    });
    res.status(201).json({ 'success': `Registro creado correctamente`, code: "PchCtr-004", status: 201 });
  } catch (error) {
    next(error);
  }
}

export const updatePunch = async (req, res, next) => {
  const { id, userId, date, punch_in, punch_out, user_name } = req.body;
  try {
    //check if params are correct
    if (!id) return res.status(400).json({ message: 'Por favor verifica la información ingresada.', code: "PchCtr-005", status: 400 });
    //check for punch on database
    const punch = await Punch.findOne({
      where: { id: id }
    });
    if (!punch) return res.status(204).json({ message: 'El usuario ingresado no fue localizado', code: "PchCtr-006", status: 204 });
    punch.userId = userId;
    punch.user_name = user_name;
    punch.date = date;
    punch.punch_in = punch_in;
    punch.punch_out = punch_out;
    const result = await punch.save();
    res.status(201).json({ 'success': `Registro actualizado correctamente`, code: "PchCtr-007", status: 201 });
  } catch (error) {
    next(error);
  }
}

export const deletePunch = async (req, res, next) => {
  const { id } = req.body;
  try {
    //check if params are correct
    if (!id) return res.status(400).json({ message: 'Por favor verifica la información ingresada.', code: "PchCtr-008", status: 400 });
    //check for punch on database
    const punch = await Punch.findOne({
      where: { id: id }
    });
    if (!punch) return res.status(204).json({ message: 'El usuario ingresado no fue localizado', code: "PchCtr-009", status: 204 });
    await punch.destroy();
    res.status(200).json({ 'success': `Registro eliminado correctamente`, code: "PchCtr-010", status: 200 });
  } catch (error) {
    next(error);
  }
}

export const uploadPunchTemplate = async (req, res, next) => {
  try {
    var error = false;
    var error_msg = '';
    let fileName = 'template_data.xlsx';
    const __dirname = path.join(__filename, "../../", "uploads", "/");
    const tmpFile = `${__dirname}${fileName}`;
    const wb = new Excel.Workbook();
    await wb.xlsx.readFile(tmpFile);
    const ws = wb.worksheets[0];
    let punchData = []
    ws.eachRow((row, rowIndex) => {
      if (rowIndex !== 1) {
        const userId = row.values[1];
        const user_name = row.values[2];
        const date = row.values[3];
        const punch_in = row.values[4];
        const punch_out = row.values[5];
        let punch_in_tmp = '';
        let punch_out_tmp = '';
        //Lets make basic validation
        if (!userId || userId === '') {
          throw new Error(`El valor del User ID en la fila ${rowIndex} es incorrecto`);
        }
        if (!user_name || user_name === '') {
          throw new Error(`El valor de User Name en la fila ${rowIndex} es incorrecto`);
        }
        if (!date || date === '' || !isValidDate(date)) {
          throw new Error(`El valor de Date en la fila ${rowIndex} es incorrecto, por favor verifica que no esté vacío y el formado sea DD/MM/YYYY`);
        }
        if (!punch_in || punch_in === '') {
          throw new Error(`El valor de Punch in en la fila ${rowIndex} es incorrecto, por favor verifica que no esté vacío`);
        } else {
          //lets get correct format
          let isValid = isValidHour(punch_in);
          if (isValid !== false) {
            punch_in_tmp = isValid;
          } else {
            throw new Error(`El valor de Punch in en la fila ${rowIndex} es incorrecto, por favor verifica que el formado sea HH:SS (24h)`);
          }
        }
        if (!punch_out || punch_out === '') {
          throw new Error(`El valor de Punch in en la fila ${rowIndex} es incorrecto, por favor verifica que no esté vacío`);
        } else {
          //lets get correct format
          let isValid = isValidHour(punch_out);
          if (isValid !== false) {
            punch_out_tmp = isValid;
          } else {
            throw new Error(`El valor de Punch in en la fila ${rowIndex} es incorrecto, por favor verifica que el formado sea HH:SS (24h)`);
          }
        }
        let rowData = {
          "userId": userId,
          "user_name": user_name,
          "date": date,
          "punch_in": punch_in_tmp,
          "punch_out": punch_out_tmp,
        }
        punchData.push(rowData)
      }
    })
    Punch.bulkCreate(punchData);
    if (error)
      res.status(500).json({ message: error_msg, code: 'PchCtr-011', status: 500 });
    else
      res.status(200).json({ 'success': `Template cargada correctamente`, code: "PchCtr-012", status: 200 });
  } catch (error) {
    next(error);
  }
}