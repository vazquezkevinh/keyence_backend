import { DataTypes } from "sequelize";
import { sequelize } from "../database/db.js"

export const Punch = sequelize.define(
  "punchs",
  {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      user_name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      date: {
        type: DataTypes.DATEONLY,
        allowNull: false
      },
      punch_in: {
        type: DataTypes.TIME,
        allowNull: false
      },
      punch_out: {
        type: DataTypes.TIME,
        allowNull: false
      }
  },
  {
    timestamps: false,
  }
);