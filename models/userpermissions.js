import { DataTypes } from "sequelize";
import { sequelize } from "../database/db.js"

export const UserPermissions = sequelize.define(
  "userpermissions",
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    usuario_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    priv_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
  },
  {
    timestamps: false,
  }
);

