import { DataTypes } from "sequelize";
import { sequelize } from "../database/db.js"
import { Punch } from './punch.js'
import { UserPermissions } from "./userpermissions.js";

export const User = sequelize.define(
  "users",
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    username: {
      type: DataTypes.STRING,
      primaryKey: true,
      allowNull: false
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: false
    }, 
    email: {
      type: DataTypes.STRING
    }, 
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }, 
    remember_token: {
      type: DataTypes.STRING,
      allowNull: true
    },
    active: {
      allowNull: true,
      defaultValue: 1,
      type: DataTypes.CHAR
    }
  },
  {
    timestamps: false,
  }
);

User.hasMany(UserPermissions, {
  foreinkey: "usuario_id"
});


/*User.hasMany(Punch, {
  foreinkey: "userId"
});

Punch.belongsTo(User, {
  through: "punchs",
  as: "userPunchs",
  foreignKey: "userId",
});
*/