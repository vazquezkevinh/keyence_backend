const verifyRoles = (...allowedRoles) => {
    return (req, res, next) => {
        if (!req?.privs) return res.sendStatus(401);
        const rolesArray = [...allowedRoles];
        const result = req.privs.map(role => rolesArray.includes(role)).find(val => val === true);
        if (!result) return res.sendStatus(401);
        next();
    }
}

export default verifyRoles