import { logEvents } from "./logEvents.js"

const errorHandler = (err, req, res, next) => {
    console.log("Getting error from error handler");
    const errStatus = err.statusCode || 500;
    const errMsg = err.message || 'Something went wrong';
    logEvents(errMsg, 'errLog.txt');
    res.status(errStatus).json({
        success: false,
        status: errStatus,
        message: errMsg,
        stack: process.env.NODE_ENV === 'development' ? err.stack : {}
    })
}

export default errorHandler;