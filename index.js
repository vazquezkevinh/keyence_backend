import app from "./server.js";
import { sequelize } from "./database/db.js";

const PORT = process.env.PORT || 3500;

async function main() {
  await sequelize.sync({force: false});
  app.listen(PORT);
  console.log(` Server Listening on port ${PORT} `);
}

main();