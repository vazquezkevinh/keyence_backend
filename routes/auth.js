import { Router } from 'express';
const router = Router();
import { handleLogin, verifyByToken } from '../controllers/authController.js';

router.post('/', handleLogin);
router.post('/verifyByToken', verifyByToken);

export default router;