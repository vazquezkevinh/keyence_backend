import { Router } from 'express';
import { getAllPunch, getPunch, updatePunch, createPunch, deletePunch, uploadPunchTemplate } from '../../controllers/punchController.js';
import { punchCreateCheck, punchUpdateCheck } from '../../validators/punchValidator.js';
import { multerUpload } from '../../middleware/multerUpload.js';
const router = Router();

router.route('/')
    .get(getAllPunch);

router.route('/:id')
    .get(getPunch);

router.route('/')
    .post(punchCreateCheck(), createPunch);

router.route('/')
    .put(punchUpdateCheck(), updatePunch);

router.route('/')
    .delete(deletePunch);

router.route('/uploadPunchTemplate')
    .post(multerUpload.single('file'), uploadPunchTemplate);

export default router;