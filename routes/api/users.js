import { Router } from 'express';
const router = Router();
import { getAllUsers, getUserData, updateUser, storeUser, verify } from '../../controllers/usersController.js';
import ROLES_LIST from '../../config/roles_list.js';
import verifyRoles from '../../middleware/verifyRoles.js';

router.route('/')
    .get(verifyRoles(ROLES_LIST.Administrador), getAllUsers);

router.route('/edit')
    .post(verifyRoles(ROLES_LIST.Administrador), getUserData);

router.route('/update')
    .post(verifyRoles(ROLES_LIST.Administrador), updateUser);

router.route('/store')
    .post(verifyRoles(ROLES_LIST.Administrador),storeUser);

router.route('/verify')
    .get(verify);

export default router;