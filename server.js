import 'dotenv/config'
import express from 'express';
const app = express();
import { dirname, join, extname } from 'path';
import { fileURLToPath } from 'url'
import cors from 'cors';
import corsOptions from './config/corsOptions.js';
import { logger } from './middleware/logEvents.js';
import errorHandler from './middleware/errorHandler.js';
import verifyJWT from './middleware/verifyJWT.js';
import cookieParser from 'cookie-parser';
import credentials from './middleware/credentials.js';

/* Routes */
import RootRoutes from './routes/root.js'
import AuthRoutes from './routes/auth.js'
import RefreshRoutes from './routes/refresh.js'
import LogoutRoutes from './routes/logout.js'
import UsersRoutes from './routes/api/users.js'
import PunchRoutes from './routes/api/punch.js'

const __dirname = dirname(fileURLToPath(import.meta.url));

// custom middleware logger
app.use(logger);

// Handle options credentials check - before CORS!
// and fetch cookies credentials requirement
app.use(credentials);

// Cross Origin Resource Sharing
app.use(cors(corsOptions));

// built-in middleware to handle urlencoded form data
app.use(express.urlencoded({ extended: false }));

// built-in middleware for json 
app.use(express.json());

//middleware for cookies
app.use(cookieParser());

//serve static files
//app.use(express.static(join(__dirname, '/public')));
app.use('/public', express.static(join(__dirname, '../uploads')));

// routes
app.use('/', RootRoutes);
app.use('/auth', AuthRoutes);
app.use('/refresh', RefreshRoutes);
app.use('/logout', LogoutRoutes);

app.use('/punch', PunchRoutes)

app.use(verifyJWT);
app.use('/users', UsersRoutes);

app.all('*', (req, res) => {
    res.status(404);
    if (req.accepts('html')) {
        res.sendFile(join(__dirname, 'views', '404.html'));
    } else if (req.accepts('json')) {
        res.json({ "error": "404 Not Found" });
    } else {
        res.type('txt').send("404 Not Found");
    }
});

app.use(errorHandler);

export default app;