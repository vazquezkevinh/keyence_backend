'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {

    await queryInterface.bulkInsert('userpermissions', [{
      usuario_id: 1,
      priv_name: 'Administrador'
    }], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('userpermissions', null, {});
  }
};
