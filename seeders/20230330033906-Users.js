'use strict';
const bcrypt = require('bcrypt');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('users', [{
      username: 'kevin',
      nombre: 'Kevin Vázquez',
      password: '$2b$10$3ZAgY5uKRTc6EI98BKeXHO1L5tqbW7.IA0zbgx8C/lcpQNNZmfYaG',
      active: 1
    }], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('users', null, {});
  }
};
