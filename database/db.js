import Sequelize from "sequelize";

export const sequelize = new Sequelize(
  process.env.DB_DATABASE, // db name,
  process.env.DB_USER, // username
  process.env.DB_PASSWORD, // password
  {
    host: process.env.DB_HOST,
    dialect: "mysql",
    logging: false,
    define: {
      timestamps: false
    },
    dialectOptions: {
      ssl: {
        require: true
      }
    }
    // pool: {
    //   max: 5,
    //   min: 0,
    //   require: 30000,
    //   idle: 10000,
    // },
    // logging: false,
  }
);