const allowedOrigins = [
    'http://localhost:5173',
    'https://keyence-backend-ww85.onrender.com/',
    'https://642b307e2600a40008644550--keyence-frontend.netlify.app',
    'https://642b307e2600a40008644550--keyence-frontend.netlify.app/',
    'https://keyence-frontend.netlify.app',
    'https://keyence-frontend.netlify.app/'
];

export default allowedOrigins;