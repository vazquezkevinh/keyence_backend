/* Regex for validate hour as format:  HH:SS (24h) */
export const hourRegex = new RegExp(/(?:\d|[01]\d|2[0-3]):[0-5]\d$/);

/* Regex for validate date as format:  DD/MM/YYYY */
export const dateRegex = new RegExp(/^\d{2}\/\d{2}\/\d{4}$/);

export const isValidDate = (date) => {
  return date instanceof Date && !isNaN(date);
}

export const isValidHour = (hour) => {
  //first lets check with regex
  const validRegex = hourRegex.test(hour);

  if(validRegex) return true;

  //If not lets check with date time instance
  const validDate = isValidDate(hour);
  
  if(!validDate) return false;

  //lets get hour and minutes from date
  return `${hour.getHours()}:${hour.getMinutes()}`
}

export const monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
  "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
];

export const monthShortNames = ["En", "Feb", "Mar", "Abr", "My", "Jun",
  "Jul", "Ag", "Sep", "Oct", "Nov", "Dic"
];